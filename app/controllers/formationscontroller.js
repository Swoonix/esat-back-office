var models = require('./../models');
var Formation = models.formation;
var User = models.user;


var exports = module.exports = {};

exports.formations = function(req, res) {
    Formation.findAll().then(formations => {
        res.render('formations', {
            formations: formations
        });
    })
}

exports.edit = function(req, res) {
    const id = req.params.id;
    Formation.find({
            where: {
                id: id
            }
        }).then(f => f.update({
            nom: req.body.nom,
            description: req.body.description,
            categorie: req.body.categorie,
            url: req.body.url
        }))
        .then(() => Formation.findAll())
        .then(formations => res.redirect('/formations'));
}

exports.create = function(req, res) {
    const formation = Formation.build({
            nom: req.body.nom,
            description: req.body.description,
            categorie: req.body.categorie,
            url: req.body.url,
            exercice: req.body.exercice,
            quizz: req.body.quizz
        }).save()
        .then(() => Formation.findAll())
        .then(formations => res.redirect('/formations'));
}

exports.delete = function(req, res) {
    const id = req.params.id;
    Formation.destroy({
            where: {
                id: id
            }
        })
        .then(() => Formation.findAll())
        .then(formations => res.redirect('/formations'));
}

exports.show = function(req, res) {
    Formation.findById(req.params.id).then(
        formation => res.render('formations/' + formation.url + ".hbs", {
            formation: formation,
            layout: false
        })
    )
}

exports.quizz = function(req, res) {
    Formation.findById(req.params.id).then(
        formation => res.render('quizz/' + formation.quizz + ".hbs", {
            formation: formation
        })
    )
}

exports.exercice = function(req, res) {
    Formation.findById(req.params.id).then(
        formation => res.render('exercices/' + formation.exercice + ".hbs", {
            formation: formation,
            layout: false
        })
    )
}