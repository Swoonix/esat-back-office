var models = require('./../models');
var Formation = models.formation;
var User = models.user;

var exports = module.exports = {}

exports.index = function(req, res) {
    res.render('index');
}

exports.signup = function(req, res) {
    res.render('index', {
        currentUser: req.user
    });
}

exports.promote = function(req, res) {
    const id = req.params.id;
    User.find({
            where: {
                id: id
            }
        })
        .then(u => u.update({
            isAdmin: true
        }))
        .then(res.redirect('/members'));
}

exports.downgrade = function(req, res) {
    const id = req.params.id;
    User.find({
            where: {
                id: id
            }
        })
        .then(u => u.update({
            isAdmin: false
        }))
        .then(res.redirect('/members'));
}

exports.edit = function(req, res) {
    const id = req.params.id;
    if (req.body.newPassword === req.body.confirmation) {
        User.find({
            where: {
                id: id
            }
        }).then(u => {
            bCrypt.compare(req.body.password, u.password, function(err, success) {
                if (success) {
                    u.update({
                        lastname: req.body.lastname,
                        firstname: req.body.firstname,
                        password: generateHash(req.body.confirmation)
                    }).then(() => res.redirect('/dashboard'));
                } else {
                    res.redirect('/dashboard');
                }
            });
        });
    } else {
        res.redirect('/formations');
    }
}

exports.dashboard = function(req, res) {
    res.render('dashboard');
}

exports.members = function(req, res) {
    User.findAll().then(users => {
        res.render('members', {
            users: users
        });

    })

}