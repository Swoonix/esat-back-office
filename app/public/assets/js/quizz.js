function init() {
    styleCard();
    // bind boutons vérifier 
    bindCheckButtons();
    // bind boutons suivant et précedents
    bindControlButtons();
}

function styleCard() {
    $(".card.question").css("margin-left", function(index) {
        return index * 3;
    });
}

function bindCheckButtons() {
    $('.button.check').click(function(elem) {
        index = $('.button.check').index(this);
        // check if radios at index validate have attr 'right-answer'
        if (checkAnswer(index)) {
            // enleve le bouton verifier
            $('button.check')[index].className += ' disappear';
            // fait apparaître le bouton pour la question suivante (& précedente si index != 0)
            $('button.next')[index].className += ' active';
            if (index > 0) {
                console.log(index);
                $('button.precedent')[index - 1].className += ' active';
            }
            // ajoute la classe sucess au message
            $('.message')[index].className += ' success'
        } else {
            // ajoute la classe error au message
            $('.message')[index].className += ' error'
        }
        // affiche le message
        $('.message')[index].className += ' active';
    })
}

function bindControlButtons() {
    // boutons suivants
    $('button.next').click(function(elem) {
            index = $('.button.next').index(this);
            // class 2-plan sur les autres cartes
            $('.ui.card.question.active').addClass('second-plan');
            // class active sur la carte suivante
            $('.ui.card.question')[index + 1].className += ' active';
        })
        // boutons précedents
    $('button.precedent').click(function(elem) {
        index = $('.button.precedent').index(this);
        // enleve class 2-plan sur la carte precedente autres cartes
        $('.ui.card.question.active')[index].classList.remove('second-plan');
        // enleve la class active sur la carte suivante
        $('.ui.card.question')[index + 1].classList.remove('active');
    })
}

function checkAnswer(index) {
    var radios = $($('.ui.card.question').get(index)).find('input');
    var ret = false;
    radios.each(function(i) {
        if (this.checked && $(this).attr('answer') == 'true') {
            ret = true;
        }
        if (this.checked && $(this).attr('answer') == 'false') {
            ret = false;
        }
    })
    return ret;
}

init();

// TODO: gerer le premier et le dernier cas (la derniere carte ne doit pas avoir de boutton)