/*
Horus HCS ESAT V1.0

Routes de l'application

    Authentification
        GET     '/'                     -> accueil
        GET     '/signup'               -> inscriptions
        GET     '/signin'               -> connexion
        POST    '/signup'               -> inscrit l'utilisateur + token        -> formations
        POST    'signin'                -> genere token puis                    -> formations
        GET     '/logout'               -> detruit token puis                   -> accueil    
    Gestion utilisateurs
        GET     '/dashboard'            -> page dashboard
        POST    '/user/edit/:id'        -> update l'utilisateur                 -> dashboard
        GET     '/members'              -> page des membres   
        GET     '/user/:id/promote      -> promeut l'utilisateur au rang d'admin
        GET     '/user/:id/downgrade    -> rétrograde l'utilisateur 
    Formation & Quizz & Exercices
        POST    '/formations'           -> nouvelle formations                  -> formations
        GET     '/formations'           -> page formations
        POST    '/formations/edit/:id'  -> update la formations                 -> formations
        GET     '/formation/destroy/:id'-> detruit la formation                 -> formations
        GET     '/formations/:id'       -> page de la formation
        GET     '/quizz/:id'            -> page de quizz de la formation
        GET     '/exercices/:id'        -> page d'exercices de la formation
    
Helpers
        generateHash : string   -> HashString salt : string
        isLoggedIn              -> poursuit si l'utilisateur est connecté, sinon    -> login
        isAdmin                 -> poursuit si l'utilisateur est admin, sinon       -> dashboard         



*/

var bCrypt = require('bcrypt-nodejs');
// Models
var models = require('./../models');
var Formation = models.formation;
var User = models.user;
// Controllers
var authController = require('../controllers/authcontroller.js');
var formationsController = require('../controllers/formationscontroller');
var userController = require('../controllers/userscontroller');





module.exports = function(app, passport) {

    app.get('/', currentUser, userController.index);

    app.get('/signup', currentUser, authController.signup);

    app.get('/signin', currentUser, authController.signin);

    app.post('/signup', passport.authenticate('local-signup', {
        successRedirect: '/formations',
        failureRedirect: '/signup'
    }));

    app.post('/signin', passport.authenticate('local-signin', {
        successRedirect: '/formations',
        failureRedirect: '/signin'
    }));

    app.get('/logout', authController.logout);

    app.post('/user/edit/:id', isLoggedIn, userController.edit);

    app.get('/members', isLoggedIn, isAdmin, userController.members);

    app.get('/user/:id/promote', isLoggedIn, isAdmin, userController.promote);

    app.get('/user/:id/downgrade', isLoggedIn, isAdmin, userController.downgrade);

    app.get('/dashboard', isLoggedIn, userController.dashboard);

    app.post('/formations', isLoggedIn, isAdmin, formationsController.create);

    app.get('/formations', isLoggedIn, formationsController.formations);

    app.post('/formations/edit/:id', isLoggedIn, isAdmin, formationsController.edit);

    app.get('/formation/destroy/:id', isLoggedIn, isAdmin, formationsController.delete);

    app.get('/formations/:id', isLoggedIn, formationsController.show);

    app.get('/quizz/:id', isLoggedIn, formationsController.quizz);

    app.get('/exercices/:id', isLoggedIn, formationsController.exercice);

    var generateHash = function(password) {
        return bCrypt.hashSync(password, bCrypt.genSaltSync(8), null);
    };

    function currentUser(req, res, next) {
        if (req.isAuthenticated()) {
            res.locals.currentUser = req.user
        }
        return next();
    }

    function isLoggedIn(req, res, next) {
        if (req.isAuthenticated()) {
            res.locals.currentUser = req.user
            return next();
        } else {
            res.redirect('/signin');
        }
    }

    function isAdmin(req, res, next) {
        if (req.user.isAdmin)
            return next();
        else
            res.redirect('/dashboard');
    }
}