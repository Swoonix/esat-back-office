module.exports = function(sequelize, Sequelize) {
    var Formation = sequelize.define('formation', {
        nom: {
            type: Sequelize.STRING
        },
        description: {
            type: Sequelize.STRING
        },
        url: {
            type: Sequelize.STRING
        },
        categorie: {
            type: Sequelize.STRING
        },
        exercice: {
            type: Sequelize.STRING
        },
        quizz: {
            type: Sequelize.STRING
        }
    });

    // force: true will drop the table  if it already exists
    Formation.sync().then(() => {
        // Table created
        return Formation.create({
            nom: 'Formation Facebook',
            description: 'Bienvenue à la formation sur Facebook, vous y apprendrez le fonctionnement et les dangers du réseau social le plus connus.',
            url: 'form1',
            categorie: 'facebook',
            exercice: 'exo1',
            quizz: 'quizz1'
        });
    });
    return Formation
}