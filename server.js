    var express = require('express')
    var app = express()
    var passport = require('passport')
    var session = require('express-session')
    var bodyParser = require('body-parser')
    var env = require('dotenv').load()
    var exphbs = require('express-handlebars')
    var hbs = require('hbs')


    //For path assets
    app.use('/semantic', express.static(__dirname + '/node_modules/semantic-ui/dist/'));
    app.use('/jquery', express.static(__dirname + '/node_modules/jQuery/tmp/'));
    app.use('/custom', express.static(__dirname + '/app/public/assets/'));


    //For BodyParser
    app.use(bodyParser.urlencoded({
        extended: true
    }));
    app.use(bodyParser.json());


    // For Passport
    app.use(session({
        secret: 'keyboard cat',
        resave: true,
        saveUninitialized: true
    })); // session secret
    app.use(passport.initialize());
    app.use(passport.session()); // persistent login sessions



    //For Handlebars
    app.set('views', './app/views')
    app.engine('hbs', exphbs({
        extname: '.hbs',
        defaultLayout: 'base',
        layoutsDir: __dirname + '/app/views/layouts/',
        partialsDir: __dirname + '/app/views/shared/'
    }));
    app.set('view engine', '.hbs');





    //Models
    var models = require("./app/models");


    //Routes
    var Routes = require('./app/routes/routes.js')(app, passport);


    //load passport strategies
    require('./app/config/passport/passport.js')(passport, models.user);


    //Sync Database
    models.sequelize.sync().then(function() {
        console.log('Nice! Database looks fine')

    }).catch(function(err) {
        console.log(err, "Something went wrong with the Database Update!")
    });



    app.listen(5000, function(err) {
        if (!err)
            console.log("ESAT est en ligne sur le port 5000");
        else console.log(err)

    });